<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Family;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $familys = Family::paginate(5);

        return view('family.index', ['familys'=>$familys]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('family.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'code'=>'required|unique:modules,code|max:255',
            'name'=>'required|unique:modules,name|max:255'
        ];

        $request->validate($rules);


        $familys =  new Family();
        $familys->fill($request->all());
        $familys->save();
        return redirect('/familys');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $family = Family::findOrFail($id);


         return view('family.show', ['family'=>$family]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $familys = Family::findOrFail($id);

        return view('family.edit' , ['familys'=>$familys]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $rules =[
            'code'=>'required|unique:modules,code|max:255',
            'name'=>'required|unique:modules,name|max:255'
        ];

        $request->validate($rules);

        $family=Family::findOrFail($id);

        $family->fill($request->all());

        $family->save();

        return redirect('/familys');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Family::destroy($id);
        return back();
    }
}
