<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Family;
use App\Study;
use Illuminate\Support\Facades\Validator;


class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Family::with('studies')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
        'code'=>'required|unique:families,code|max:255',
        'name'=>'required|unique:families,name|max:255'
    ];

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
        return response()->json($validator->errors(),400);
    }

    $request->validate($rules);

    $family = new Family;
    $family->fill($request->all());
    $family->save();
    return $family;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         return Family::with('studies')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $family =Family::with('studies')->find($id);

        if(!$family){
            return response()->json([
                'mesage' =>'no encontrado',
            ],404);
        }

        $family->fill($request->all());
        $family->save();
        $family->refresh(); //para actualizar los objetos relacionados

        return $family;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Family::destroy($id);
        return response()->json([
            'message'=>'se ha borrado',
        ],201);
    }
}
