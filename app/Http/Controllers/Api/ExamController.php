<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exam;
use App\User;
use App\Module;
use Illuminate\Support\Facades\Validator;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return  User::all();

        return Exam::with('user')->get();
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $exam = new Exam;
        $rules = [
            'title' => 'required|max:255|unique:exams,title,' . $exam->id,
            'date' => 'required|date',
            'user_id' => 'exists:users,id',
            'module_id' => 'exists:modules,id'
        ];
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            //return $validator->errors(); esta da status 200. mejor el de abajo
            return response()->json($validator->errors(),400);
        }

        $request->validate($rules);
        $exam->fill($request->all());
        //$exam->user_id = \Auth::user()->id; no nos hace falta en api porque no esta logueado
        $exam->save();
        //return redirect('/exams'); no tiene sentido en este api
        return $exam;
    }

    public function show($id)
    {

        $exam = Exam::with('user', 'module', 'questions')->find($id);
        if($exam){
            return $exam;
        }else{
            return response()->json(['message'=>'Record not found',],404);
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $exam =Exam::with('user', 'module')->find($id);

        if(!$exam){
            return response()->json([
                'mesage' =>'no encontrado',
            ],404);
        }

        $exam->fill($request->all());

        $exam->save();
        $exam->refresh(); //para actualizar los objetos relacionados

        return $exam;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Exam::destroy($id);
        return response()->json([
            'message'=>'se ha borrado',
        ],201);
    }
}
