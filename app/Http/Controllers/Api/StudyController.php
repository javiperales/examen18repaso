<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Study;
use App\Family;
use Illuminate\Support\Facades\Validator;

class StudyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Study::with('family')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $rules =[
            'name'=>'required|unique:studies,code|max:255',
            'code'=>'required|unique:studies,name|max:255',
           'family_id' =>'required',
        ];

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
        return response()->json($validator->errors(),400);
    }

    $request->validate($rules);

    $study = new Study;
    $study->fill($request->all());
    $study->save();
    return $study;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         return Study::with('family')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $study =Study::with('family')->find($id);

        if(!$study){
            return response()->json([
                'mesage' =>'no encontrado',
            ],404);
        }

        $study->fill($request->all());
        $study->save();
        $study->refresh(); //para actualizar los objetos relacionados

        return $study;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Study::destroy($id);
        return response()->json([
            'message'=>'se ha borrado',
        ],201);
    }
}
