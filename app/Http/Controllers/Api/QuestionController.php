<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Module;
use App\Question;
use App\Examn;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); //lleva a l ventana de registro
        //$this->middleware('guest'); ruta para invitiados , lleva al home
        //->except(ruta) para que se aplique a todos menos a una ruta
        //->only(ruta) para que se aplique a una ruta o a varias
    }

    public function index()
    {
        return Question::with('exams', 'module')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $rules=[
            'text'=>'required|max:255',
            'a'=>'required|max:255',
            'b'=>'required|max:255',
            'c'=>'required|max:255',
            'd'=>'required|max:255',
            'answer'=>'required|max:1',
            'module_id'=>'required'

        ];
    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
        return response()->json($validator->errors(),400);
    }

    $request->validate($rules);

        $question = new Question;
        $question->fill($request->all());
        $question->save();
        return $question;
    }

    public function show($id)
    {
        return Question::with('exams', 'module')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $question =Question::with('exams' ,'module')->find($id);

        if(!$question){
            return response()->json([
                'mesage' =>'no encontrado',
            ],404);
        }

        $question->fill($request->all());
        $question->save();
        $question->refresh(); //para actualizar los objetos relacionados

        return $question;
    }
    public function destroy($id)
    {
        Question::destroy($id);
        return response()->json([
            'message'=>'se ha borrado',
        ],201);
    }
}
