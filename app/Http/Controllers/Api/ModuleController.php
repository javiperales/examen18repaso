<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exam;
use App\Question;
use App\Module;
use Illuminate\Support\Facades\Validator;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Module::with('exams', 'questions')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $rules =[
        'code'=>'required|unique:modules,code|max:255',
        'name'=>'required|unique:modules,name|max:255'
    ];

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){
        return response()->json($validator->errors(),400);
    }

    $request->validate($rules);

    $module = new Module;
    $module->fill($request->all());
    $module->save();
    return $module;
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Module::with('exams', 'questions')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $module =Module::with('exams' ,'questions')->find($id);

        if(!$module){
            return response()->json([
                'mesage' =>'no encontrado',
            ],404);
        }

        $module->fill($request->all());
        $module->save();
        $module->refresh(); //para actualizar los objetos relacionados

        return $module;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Module::destroy($id);
        return response()->json([
            'message'=>'se ha borrado',
        ],201);
    }
}
