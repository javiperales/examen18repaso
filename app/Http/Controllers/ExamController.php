<?php

namespace App\Http\Controllers;

use App\Question;
use App\Module;
use App\Exam;
use App\User;
use Illuminate\Http\Request;
use Session;
use Auth;
use DB;

use  Illuminate\Database\QueryException;


class ExamController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exams = Exam::paginate(15);
        return view('exam.index', ['exams' => $exams]);
        //return Exam::with('user')->get();
    }

    /*
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return Session::get('module');
        $modules = Module::all();
        return view('exam.create', ['modules' => $modules]);
    }
/*
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
public function store(Request $request)
{
    $rules=[
        'title'=> 'required|unique:exams,title|max:255',
        'module_id'=>'exists:modules,id',
        /*'user_id'=>'exists:users, id',*/
        'date'=>'date'
    ];

    $request->validate($rules);

        //opcion 1
    $exam = new Exam;
    $exam->fill($request->all());
    $exam->user_id = \Auth::user()->id;
    $exam->save();

    return redirect('/exams');

        //$flight = new Fligh;
        //$fligth->name = $request->name;
        //$fligh->save();
        //$flight = App\Fligh::create(['name' => 'fligh 10']);
        //return view('exam.index', ['exams' => $exams]);

        //opcion 2
        //$flight = App\flight::create($request->all());
}

    /*
     * Display the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exam = Exam::findOrFail($id);
        $questions = Question::all();
        return view('exam.show', ['exam' => $exam , 'questions' => $questions]);
        //return Exam::with('user', 'module', 'questions')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exam = Exam::findOrFail($id);
        $users = User::all();
        $modules = Module::all();

        return view('exam.edit', [
            'exam' => $exam,
            'users' => $users,
            'modules' => $modules
        ]);
    }

    public function update(Request $request, $id)
    {
        $rules=[
            'title'=> 'required|max:255|unique:exams,title,' .$id,
            'module_id'=>'exists:modules,id',
            /*'user_id'=>'exists:users, id',*/
            'date'=>'date'
        ];

        $request->validate($rules);

        $exam = Exam::findOrFail($id);
        $exam->fill($request->all());
        $exam->save();

        return redirect("/exams/$id");
    }

    /*
     * Remove the specified resource from storage.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $exam =Exam::findOrFail($id);
        $this->authorize('delete',$exam);
        $exam->delete();
        return back();
    }

    public function remember($id, Request $request)
    {
        $exam = Exam::findOrFail($id);
        $request->session()->put('exam', $exam);
        return back();
    }

    public function forget( Request $request)
    {
        //al olvidar no hace falta tener el id

        $request->session()->forget('exam');
        return back();
    }

    public function new()
    {
        if(Session::has('module')){
            $module = Session::get('module');  //meter modulo en sesion
            $questions = Question::where('module_id', $module->id)->paginate(); //mostrar las preguntas cuales el modulo sea igual al id de ese modulo
            return view('exam.new',['module'=>$module , 'questions'=>$questions ]);
        }else{

            $modules = Module::all();
            return view('exam.create', ['modules' => $modules]);
        }

        //si hay module en session
        //vista de eleccion de preguntas
        //si no
        //vista de eleccion de module

        //si hay modulo, lista de preguntas para elegir
        //post /exams/setmodule
            //-enviamos modulo para guardar en sesion
            //get /exams/reset
                //olvidmos el modulo y preguntas y volvemos al principio
        //get /exams/questions/{id}
            //- añadimos /quitamos la pergunta segun el id

    }

    public function newSetModule(Request $request)
    {
        $module = Module::find($request->module_id);
        Session::put('module' ,$module);
        Session::put('title', $request->title);
        Session::put('date', $request->date);

        return redirect ('/exams/new');

    }


    public function switchQuestion($id, Request $request)
    {
      $question = Question::findOrFail($id);
      $questions = Session::get('questions');

    if(isset($questions[$question->id])){
        unset($questions[$id]);
    }else{
        $questions[$id] =$question;
    }

   // $request->session()->put('question', $question);
    Session::put('questions' , $questions);
    return back();
}

public function forgetQuestion(Request $request)
{
    $request->session()->forget('question');
    return back();
}

public function save(Request $request)
{
    DB::beginTransaction();
    // try {
        $exam = new Exam;
        $exam->user_id = Auth::user()->id;
        $exam->module_id = Session::get('module')->id;
        $exam->title = Session::get('title');
        $exam->date = Session::get('date');
        $exam->save();
        foreach (Session::get('questions') as $question) {
            $exam->questions()->attach($question->id);
        }

        // $exam->questions()->attach(985989898);
//
        //forget
        Session::forget('module');
        Session::forget('title');
        Session::forget('date');
        Session::forget('questions');
        DB::commit();
        return redirect ('/exams/' . $exam->id);
    // } catch (QueryException $e) {
    //     // DB::rollback();
    //     $request->session()->flash('status', 'El alta falla!');
        // return back();
    // }

}

}
