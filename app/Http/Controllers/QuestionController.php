<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Module;
use App\Exam;



class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $questions =Question::paginate(15);

        return view('question.index', ['questions'=>$questions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modules =Module::all();
        return view('question.create' , ['modules'=>$modules]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reglas=[
            'text'=>'required|max:255',
            'a'=>'required|max:255',
            'b'=>'required|max:255',
            'c'=>'required|max:255',
            'd'=>'required|max:255',
            'answer'=>'required|max:1',
            'module_id'=>'required'

        ];

        $request->validate($reglas);

        $question = new Question;
        $question->fill($request->all());

        $question->save();

        return redirect('/questions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questions =Question::findOrFail($id);

        return view('question.show', ['questions'=>$questions]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $question=Question::findOrFail($id);
        $modules = Module::all();

        return view('question.edit', ['question'=>$question , 'modules'=>$modules]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $reglas=[
            'text'=>'required|max:255',
            'a'=>'required|max:255',
            'b'=>'required|max:255',
            'c'=>'required|max:255',
            'd'=>'required|max:255',
            'answer'=>'required|max:1',
            'module_id'=>'required'

        ];

        $request->validate($reglas);

        $question = Question::findOrFail($id);
        //buscar el bojeto
        $question->fill($request->all());
        //modificar el atributo por atributo con fill
        $question->save();
        //guardar los cambios
        return redirect('/questions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     Question::destroy($id);
     return back();
 }

}
