<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;
use App\Exam;



class ModuleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

       // $this->authorize('index', Module::class); //hacer peticion sobre el modelo modules
        $user = \Auth::user();
        $modules = Module::paginate(5);
        if($user->can('index', Module::class)){
            return view('module.index', ['modules'=>$modules]);
        }else{
            //return view('errors.403');
        }


       // return view('module.index', ['modules'=>$modules]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('module.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'code'=>'required|unique:modules,code|max:255',
            'name'=>'required|unique:modules,name|max:255'
        ];

        $request->validate($rules);


        $modules =  new Module();
        $modules->fill($request->all());
        $modules->save();
        return redirect('/modules');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $module = Module::findOrFail($id);
         $this->authorize('view', $module);

         return view('module.show', ['module'=>$module]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modules=Module::findOrFail($id);


        return view('module.edit', ['modules'=>$modules]);
    }


    public function update(Request $request, $id)
    {

        $rules =[
            'code'=>'required|unique:modules,code|max:255',
            'name'=>'required|unique:modules,name|max:255'
        ];

        $request->validate($rules);

        $module=Module::findOrFail($id);

        $module->fill($request->all());

        $module->save();

        return redirect('/modules');
    }


    public function destroy($id)
    {
     Module::destroy($id);
     return back();
 }
}
