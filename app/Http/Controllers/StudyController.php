<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Study;
use App\Family;
use App\Module;



class StudyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $studies = Study::paginate(10);


        return view('study.index', ['studies'=>$studies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $studies = Study::all();
        $familys = Family::all();
        return view('study.create' , ['studies'=>$studies , 'familys'=>$familys]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'name'=>'required|unique:studies,code|max:255',
            'code'=>'required|unique:studies,name|max:255',
            'family_id' =>'required',
        ];

        $request->validate($rules);


        $studies =  new Study();
        $studies->fill($request->all());
        $studies->save();
        return redirect('/studies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $study = Study::findOrFail($id);
        $modules = Module::all();

        return view('study.show', ['study'=>$study , 'modules'=>$modules]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $studies = Study::findOrFail($id);
        $familys = Family::all();
        return view('study.edit', ['studies'=>$studies , 'familys'=>$familys]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules =[
            'name'=>'required|unique:studies,code|max:255',
            'code'=>'required|unique:studies,name|max:255',
            'family_id' =>'required',
        ];

        $request->validate($rules);

        $studies=Study::findOrFail($id);

        $studies->fill($request->all());

        $studies->save();

        return redirect('/studies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Study::destroy($id);

        return back();
    }

    public function attachmodule(Request $request ,$id)
    {

        $rules =[
            'course'=>'required|integer|max:2|min:1',
            'module_id'=>'required'
        ];

        $request->validate($rules);

        $study = Study::find($id);
        $module_id = $request->input('module_id'); //coger la variable id del formulario de show
        $course = $request->input('course');  //coger la variable curso del formulario de show
        // dd($module_id);

        $study->modules()->syncWithoutDetaching([$module_id => ['course' => $course]]); //pasar array (no va con coma)

        return back();
        //return redirect('/studies');

    }

    public function detachmodule(Request $request ,$id)
    {
        $study=Study::find($id);
        $module_id=$request->input('module_id');
        $study->modules()->detach($module_id);
        return back();
    }

}
