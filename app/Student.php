<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable=['name','apellidos', 'fecha_nacimiento' ,'direccion' ,'email' ];

    protected $dates = ['fecha_nacimiento'];
}
