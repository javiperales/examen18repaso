<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('exams', 'Api\ExamController')->except(['create', 'edit']);
Route::resource('questions', 'Api\QuestionController');
Route::resource('users', 'Api\UserController');
Route::resource('modules', 'Api\ModuleController');
Route::resource('familys', 'Api\FamilyController');
Route::resource('studies', 'Api\StudyController');
