<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/exams/switchQuestion/{id}', 'ExamController@switchQuestion');
Route::get('/exams/new', 'ExamController@new');


Route::get('/exams/save', 'ExamController@save');
Route::post('/exams/new', 'ExamController@newSetModule');
Route::get('/exams/forget', 'ExamController@forget');
Route::get('/exams/forgetQuestion', 'ExamController@forgetQuestion');
Route::resource('/exams', 'ExamController');
Route::get('/exams/{id}/remember', 'ExamController@remember');

Route::resource('/questions', 'QuestionController');
Route::resource('/familys', 'FamilyController');
Route::resource('/studies', 'StudyController');


Route::resource('/modules', 'ModuleController')->middleware('auth');
Route::resource('/users', 'UserController');
Route::resource('/students', 'StudentController');
Route::resource('exams', 'ExamController');
Route::get('/exams/{id}/remember', 'ExamController@remember');



//Route::resource('/questions', 'QuestionController');



Route::get('users', 'UserController@index');

Route::get('users/{id}', 'UserController@show');
Route::get('modules/{id}', 'ModuleController@show');
Route::get('questions/{id}', 'QuestionContrsoller@show');

Route::delete('/studies/{id}/modules' , 'StudyController@detachmodule');
Route::post('/studies/{id}/modules' , 'StudyController@attachmodule');





