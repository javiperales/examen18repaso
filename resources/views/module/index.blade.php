@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">lista de modulos</h1>
<a class="btn btn-primary" href="/modules/create">Crear un nuevo modulo</a>
<table class="table">
  <tr>
    <td>nombre</td>
    <td>codigo</td>
    <td>acciones</td>
    <td>borrar</td>

  </tr>
  @foreach($modules as $module)
  <tr>
    <td> {{$module->name}}</td>
    <td>{{$module->code}}</td>

    <td>
      @can('view' ,$module)
        <a class="glyphicon glyphicon-eye-open" href="/modules/{{$module->id}}"></a>
      @endcan
        <a class="glyphicon glyphicon-pencil" href="/modules/{{$module->id }}/edit"></a>
    </td>


    <td>
      <form method="post" action="/modules/{{$module->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="delete">
        <input type="submit" value="borrar {{$module->id}}" class="btn btn-danger">
      </form>
    </td>
  </tr>
  @endforeach
</table>
<div>
  {{ $modules->render() }}
</div>

@endsection
