@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">detalle de modulo {{$module->id}} </h1>
<ul class="list-group">
    <li class="list-group-item">codigo: {{$module->code}}</li>
    <li class="list-group-item">nombre: {{$module->name}}</li>
</ul>
<hr>
<h5 class="alert alert-info">titulo del examen</h5>
<ul class="list-group">
@foreach($module->exams as $exam)
    <li class="list-group-item">{{$exam->title}}</li>
@endforeach
</ul>

@endsection
