@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">actualizacion de modulos</h1>
<div class="container">
    <form class="form" method="post" action="/modules/{{$modules->id}}">
        <input type="hidden" name="_method" value="put">
        {{csrf_field()}}
        <div class="form-group">
            <label>nombre</label>
            <input class="form-control" type="text" name="name" value="{{$modules->name}}">
        </div>
        <div class="form-group">
        <label>codigo</label>
        <input class="form-control" type="text" name="code" value="{{$modules->code}}">
    </div>
        <input class="btn btn-primary" type="submit" name="nuevo" value="nuevo">
    </form>
</div>
@endsection
