
@extends('layouts.app')

@section('title', 'Studies')

@section('content')
<h1>
 estudio <?php echo $study->id ?>
</h1>

<ul>
    <li>Study: {{$study->name}} </li>
    <li>Code: {{$study->code}}</li>
    <li>Family: {{$study->family->name}}</li>


</ul>

<h2>
 lista de modulos y cursos
</h2>

<ul>
    @foreach ($study->modules as $module)
    <li> Course : {{ $module->pivot->course }} - {{ $module->name }}
     <form method="post" action="/studies/{{$study->id}}/modules">
         {{ csrf_field() }}
         <input type="hidden" name="_method" value="delete">
         <input type="hidden" name="module_id" value="{{$module->id}}">
         <input type="submit" value="Destroy" class="btn btn-danger"  role="button">


     </form>
 </li>

 @endforeach
</ul>


<h2>
 cursos y modulos
</h2>
<ul>
   <form class="form"  method="post" action="/studies/{{$study->id}}/modules">
    {{ csrf_field() }}

    <div class="form-group">
       <label>Course</label>
       <input class="form-control" type="" name="course" value="{{old('course')}}" >
   </div>

   @if( $errors->first('course'))
   <script>
    alert('el campo curso es obligatorio');
</script>

<div class="alert alert-danger">{{$errors->first('course')}}</div>
@endif

<div class="form-group">
    <label>Modules</label>
    <select class="form-control" type="text" name="module_id" >
        <option></option>
        @foreach($modules as $module)
        <option value="{{$module->id}}" {{old('module_id')==$module->id ? 'selected="selected"' : ''}}>{{$module->name}}</option>
        @endforeach
    </select>

    @if( $errors->first('module_id'))
   <script>
        alert('el campo modulo es obligatorio');
    </script>

<div class="alert alert-danger">{{$errors->first('module_id')}}</div>
@endif


</div>

<input type="submit" value="agregar" class="btn btn-success"  role="button">

</form>

</ul>
<a href="/studies" class="btn btn-success"  role="button">volver</a>


@endsection
