@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">lista de familias</h1>
<a class="btn btn-primary" href="/studies/create">crear nuevo estudio </a>
<table class="table">
  <tr>
    <td>nombre</td>
    <td>code</td>
    <td>nombre de familia</td>
    <td>acciones</td>
    <td>borrar</td>

  </tr>

  @foreach($studies as $study)
  <tr>
    <td>{{$study->name}}</td>
    <td>{{$study->code}}</td>
    <td>{{$study->family->name}}</td>
    <td> <a class="glyphicon glyphicon-eye-open" href="/studies/{{ $study->id }}"></a> <a class="glyphicon glyphicon-pencil" href="/studies/{{$study->id}}/edit"> </a></td>
    <td>
       <form method="post" action="/studies/{{$study->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="delete">
        <input type="submit" value="borrar {{$study->family->name}}" class="btn btn-danger">
      </form>
    </td>

    @endforeach
  </tr>
</table>

<div>{{ $studies->render() }} </div>
@endsection
