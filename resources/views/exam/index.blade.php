@extends('layouts.app')

@section('content')
<a class="btn btn-primary" href="/exams/create">Crear nuevo examen</a>
<h1>lista de examenes</h1>
   @if (Session::has('exam'))
      Examen recordado: {{ Session::get('exam')->title }}
      <a class="btn btn-warning" href="/exams/forget">Olvidar</a>
      @endif

  <table class="table table-hover">
  <tr>
    <td>titulo</td>
    <td>fecha</td>
    <td>modulo</td>
    <td>usuario</td>
    <td>acciones</td>
    <td>borrar</td>

  </tr>
  @foreach($exams as $examen)
  <tr>
    <td>{{ $examen->title }}</td>
    <td>{{ $examen->date }}</td>
    <td>{{ $examen->module->name }}</td>
    <td>{{ $examen->user->name }}</td>

    <td><a class="glyphicon glyphicon-eye-open" href="/exams/{{$examen->id }}"></a> <a class="glyphicon glyphicon-pencil" href="/exams/{{$examen->id }}/edit"></a> <a class=" glyphicon glyphicon-plus" href="/exams/{{$examen->id }}/remember"></a> </td>
    <td>
       @can('delete', $examen)
            <form method="post" action="/exams/{{$examen->id}}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="delete">
                <input type="submit" value="borrar {{$examen->id}}" class="glyphicon glyphicon-trash">
            </form>
       @endcan
        </td>

    @endforeach
  </tr>
  </table>
  <div>
    {{ $exams->render() }}

  </div>

 @endsection
