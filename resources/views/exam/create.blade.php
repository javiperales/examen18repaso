@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">creacion de examen</h1>
<div class="container">
    <form class="form" method="post" action="/exams/new">
        {{csrf_field()}}
        <div class="form-group">
            <label>titulo</label>
            <input class="form-control" type="text" name="title" value="{{ old('title') }}">
            @if( $errors->first('title'))
            <script>
                alert('el campo titulo es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('title')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>fecha</label>
            <input class="form-control" type="date" name="date" value="{{ old('date') }}">

            @if( $errors->first('date'))
            <script>
                alert('el campo fecha es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('date')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>modulo</label>
            <select class="form-control" type="text" name="module_id">
                <option></option>
                @foreach($modules as $module)
                <option value="{{$module->id}}" {{old('module_id')==$module->id ? 'selected="selected"' : ''}}>{{$module->name}}</option>
                @endforeach
            </select>

            @if( $errors->first('module_id'))
            <script>
                alert('el campo modulo es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('module_id')}}
            </div>
            @endif
            <input type="submit" name="siguiente" class="btn btn-primary">

        </div>



    </form>
</div>
@endsection
