@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">edicion de examen</h1>
<div class="container">
    <form class="form" method="post" action="/exams/{{$exam->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="put"> {{-- falsear el post para que sea put para hacer update --}}
        <div class="form-group">
            <label>titulo</label>
            <input class="form-control" type="text" name="title" value="{{$exam->title}}">
        </div>
        <div class="form-group">
            <label>modulo</label>
            <select class="form-control" type="text" name="module_id">
                <option></option>
                @foreach($modules as $module)
                <option value="{{$module->id}}"{{$exam->module_id==$module->id ? 'selected="selected"' : ''}} >{{$module->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>usuario</label>
            <select class="form-control" type="text" name="user_id">
            <option></option>
            @foreach($users as $user)
            <option value="{{$user->id}}" {{$exam->user_id==$user->id ? 'selected="selected"' : ''}}>{{$user->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>fecha</label>
        <input class="form-control" type="date" name="date" value="{{$exam->date}}">
    </div>
    <input class="btn btn-primary" type="submit" name="nuevo" value="nuevo">
</form>
</div>
@endsection
