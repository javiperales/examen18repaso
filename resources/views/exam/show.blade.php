@extends('layouts.app')

@section('content')

<h1 class="alert alert-info">detalle de examen {{$exam->id}}</h1>
<ul class="list-group">
  <li class="list-group-item">titulo:{{$exam->title}} </li>
  <li class="list-group-item">fecha: {{$exam->date}} </li>
  <li class="list-group-item">materia: {{$exam->module->name}}</li>
</ul>
<h3 class="alert alert-info">preguntas del examen</h3>
<table class="table">
  <tr>
    <td>text</td>
    <td>a</td>
    <td>b</td>
    <td>c</td>
    <td>d</td>
    <td>respuesta</td>
  </tr>


  @foreach($exam->questions as $question){{--  --}}
  <tr>
   <td>{{$question->text}}</td>
   <td class="{{$question->answer == 'a' ? 'bg-success' : ''}}">{{$question->a}}</td>
   <td class="{{$question->answer == 'b' ? 'bg-success' : ''}}">{{$question->b}}</td>
   <td class="{{$question->answer == 'd' ? 'bg-success' : ''}}">{{$question->c}}</td>
   <td class="{{$question->answer == 'd' ? 'bg-success' : ''}}">{{$question->d}}</td>
   <td >{{$question->answer}}</td>
   @endforeach
 </tr>
</table>
@endsection
