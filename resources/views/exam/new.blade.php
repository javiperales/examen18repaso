@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">

      <h1>Alta de examen</h1>

      @if (Session::has('status'))
        <div class="alert alert-danger">
          {{ Session::get('status') }}
        </div>
      @endif
      <h4>Módulo: {{ $module->name }}</h4>
      <h4>Título: {{ Session::get('title') }}</h4>
      <h4>Fecha:
      {{ date('d-m-Y', strtotime(Session::get('date'))) }}
      </h4>
      <a class="btn btn-danger" href="/exams/reset">
        Reiniciar alta
      </a>
      <h3>Preguntas ya elegidas</h3>
      <ul>
      @if (Session::has('questions'))
        @foreach(Session::get('questions') as $question)
          <li>{{ $question->text }} <a class="btn btn-warning" href="/exams/switchQuestion/{{ $question->id }}">olvidar</a></li>
        @endforeach
      @endif
      </ul>

      <h3>Elegir preguntas</h3>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>
              Enunciado
            </th>
            <th>A</th>
            <th>B</th>
            <th>C</th>
            <th>D</th>

          </tr>
        </thead>
        <tbody>
          @foreach($questions as $question)
          <tr>
            <td>
              {{ $question->module->name }}
            </td>
            <td>
              {{ $question->text }}
            </td>
            <td class="
              @if($question->answer == 'a')
                bg-success
              @endif
            ">
              {{ $question->a }}
            </td>
            <td class="{{ $question->answer == 'b' ? 'bg-success' : '' }}">
              {{ $question->b }}
            </td>
            <td class="{{ $question->answer == 'c' ? 'bg-success' : '' }}">
              {{ $question->c }}
            </td>
            <td class="{{ $question->answer == 'd' ? 'bg-success' : '' }}">
              {{ $question->d }}
            </td>
            {{--
            <td>
              {{ $question->answer }}
            </td>
            --}}
            <td>
              @if (isset(Session::get('questions')[$question->id]))
                <a class="btn btn-warning" href="/exams/switchQuestion/{{ $question->id }}">Quitar
                </a>
              @else
                <a class="btn btn-success" href="/exams/switchQuestion/{{ $question->id }}">Poner
                </a>
              @endif
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>
      <a class="btn btn-primary" href="/exams/save">Crear Examen</a>
      <br>
      lista de paginas:
      {{ $questions->render() }}


    </div>
  </div>
</div>
@endsection
