@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">lista de familias</h1>
<a class="btn btn-primary" href="/familys/create">crear nueva familia</a>
<table class="table">
  <tr>
    <td>nombre</td>
    <td>code</td>
    <td>borrar</td>
  </tr>

  @foreach($familys as $family)
  <tr>
    <td>{{$family->name}}</td>
    <td>{{$family->code}}</td>
    <td> <a class="glyphicon glyphicon-eye-open" href="/familys/{{ $family->id }}"></a> <a class="glyphicon glyphicon-pencil" href="/familys/{{$family->id}}/edit"></a></td>
    <td>
       <form method="post" action="/familys/{{$family->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="delete">
        <input type="submit" value="borrar {{$family->id}}" class="btn btn-danger">
      </form>


    </td>

    @endforeach
  </tr>
</table>

<div>{{ $familys->render() }} </div>
@endsection
