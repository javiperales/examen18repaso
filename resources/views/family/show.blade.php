@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">detalle de la familia {{$family->id}} </h1>
<ul class="list-group">
    <li class="list-group-item">codigo: {{$family->code}}</li>
    <li class="list-group-item">nombre: {{$family->name}}</li>
</ul>
<hr>
<h5 class="alert alert-info">nombre de cursos de esta familia</h5>
<ul class="list-group">
@foreach($family->studies as $study)
    <li class="list-group-item">{{$study->name}}</li>
@endforeach
</ul>

@endsection
