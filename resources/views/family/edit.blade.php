@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">actualizacion de familias</h1>
<div class="container">
    <form class="form" method="post" action="/familys/{{$familys->id}}">
        <input type="hidden" name="_method" value="put">
        {{csrf_field()}}
        <div class="form-group">
            <label>nombre</label>
            <input class="form-control" type="text" name="name" value="{{$familys->name}}">
        </div>
        <div class="form-group">
        <label>codigo</label>
        <input class="form-control" type="text" name="code" value="{{$familys->code}}">
    </div>
        <input class="btn btn-primary" type="submit" name="nuevo" value="nuevo">
    </form>
</div>
@endsection

