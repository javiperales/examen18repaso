@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">lista de usuarios</h1>
  <table class="table">
    <tr>
    <td>nombre</td>
    <td>email</td>
    <td>acciones</td>
    </tr>

    @foreach($users as $usuario)
    <tr>
      <td>{{$usuario->name}}</td>
      <td>{{$usuario->email}}</td>
      <td> <a class="glyphicon glyphicon-eye-open" href="/users/{{ $usuario->id }}"></a></td>

    @endforeach
    </tr>
  </table>
@endsection
