@extends('layouts.app')

@section('content')
<h1 class="alert alert-info" >detalle del usuario {{$users->id}}</h1>

<ul class="list-group">
    <li class="list-group-item">nombre: {{$users->name}}</li>
    <li class="list-group-item">email: {{$users->email}}</li>
</ul>
<h3 class="alert alert-info">lista de examenes</h3>
<table class="table">
   <tr>
       <td>titulo</td>
       <td>fecha</td>
       <td>materia</td>
   </tr>
   @foreach($users->exams as $exam)
   <tr>
    <td>{{$exam->title}}</td>
    <td>{{$exam->date}}</td>
    <td>{{$exam->module->name}}</td>

    @endforeach
</tr>
</table>

@endsection
