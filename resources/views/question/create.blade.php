@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">creacion de examen</h1>
<div class="container">
    <form class="form" method="post" action="/questions">
        {{csrf_field()}}
        <label>modulo</label>
        <select class="form-control" type="text" name="module_id">
            <option></option>
            @foreach($modules as $module)
            <option value="{{$module->id}}" {{old('module_id')==$module->id ? 'selected="selected"' : ''}}>{{$module->name}}</option>
            @endforeach
        </select>


        <div class="form-group">
            <label>text</label>
            <input class="form-control" type="text" name="text" value="{{old('text')}}">

             @if( $errors->first('text'))
            <script>
                alert('el campo texto es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('text')}}
            </div>
            @endif

        </div>
        <div class="form-group">

        </div>
        <div class="form-group">
            <label>a</label>
            <input class="form-control" type="text" name="a" value="{{old('a')}}">

                 @if( $errors->first('a'))
            <script>
                alert('el campo a es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('a')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>b</label>
            <input class="form-control" type="text" name="b" value="{{old('b')}}">

             @if( $errors->first('b'))
            <script>
                alert('el campo b es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('b')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>c</label>
            <input class="form-control" type="text" name="c" value="{{old('c')}}">

             @if( $errors->first('c'))
            <script>
                alert('el campo c es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('c')}}
            </div>
            @endif


        </div>

        <div class="form-group">
            <label>d</label>
            <input class="form-control" type="text" name="d" value="{{old('d')}}">

             @if( $errors->first('d'))
            <script>
                alert('el campo d es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('d')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>respuesta</label>
            <input class="form-control" type="text" name="answer" value="{{old('answer')}}">

                 @if( $errors->first('answer'))
            <script>
                alert('el campo respuesta es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('answer')}}
            </div>
            @endif


        </div>
        <input class="btn btn-primary" type="submit" name="nuevo" value="nuevo">
    </form>
</div>
@endsection
