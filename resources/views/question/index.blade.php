@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">lista de preguntas</h1>
<a class="btn btn-primary" href="/questions/create">Crear una nueva pregunta</a>
<table class="table">
    <tr>
        <td>texto</td>
        <td>nombre modulo</td>
        <td>acciones</td>
        <td>borrar</td>
    </tr>
    @foreach($questions as $question)
    <tr>
        <td>{{$question->text}}</td>
        <td>{{$question->module->name}}</td>
        <td><a class="glyphicon glyphicon-eye-open" href="/questions/{{$question->id}}"></a> <a class="glyphicon glyphicon-pencil" href="/questions/{{$question->id }}/edit"></a></td>
        <td>
            <form method="post" action="/questions/{{$question->id}}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="delete">
                <input type="submit" value="borrar {{$question->id}}" class="btn btn-danger">
            </form>
        </td>

        @endforeach
    </tr>

</table>
<div>{{ $questions->render() }}</div>

@endsection
