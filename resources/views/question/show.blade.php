@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">detalle de la pregunta {{$questions->id}}</h1>

<ul class="list-group">
    <li class="list-group-item">pertenece a: {{$questions->module->name}}</li>
</ul>

<h3 class="alert alert-info">lista de examenes asociados</h3>
<ul class="list-group">
    @foreach($questions->exams as $exam)
    <li class="list-group-item">{{$exam->title}}</li>
    @endforeach
</ul>
@endsection
