@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">actualizacion de preguntas</h1>
<div class="container">
    <form class="form" method="post" action="/questions/{{$question->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="put">
        <label>modulo</label>
        <select class="form-control" type="text" name="module_id">
            <option></option>
            @foreach($modules as $module)
             <option value="{{$module->id}}"{{$question->module_id==$module->id ? 'selected="selected"' : ''}} >{{$module->name}}</option>
            @endforeach
        </select>


        <div class="form-group">
            <label>text</label>
            <input class="form-control" type="text" name="text" value="{{$question->text}}">
        </div>
        <div class="form-group">

        </div>
        <div class="form-group">
            <label>a</label>
            <input class="form-control" type="text" name="a" value="{{$question->a}}">
        </div>

        <div class="form-group">
            <label>b</label>
            <input class="form-control" type="text" name="b" value="{{$question->b}}">
        </div>

        <div class="form-group">
            <label>c</label>
            <input class="form-control" type="text" name="c" value="{{$question->c}}">
        </div>

        <div class="form-group">
            <label>d</label>
            <input class="form-control" type="text" name="d" value="{{$question->d}}">
        </div>

        <div class="form-group">
            <label>respuesta</label>
            <input class="form-control" type="text" name="answer" value="{{$question->answer}}">
        </div>
        <input class="btn btn-primary" type="submit" name="nuevo" value="nuevo">
    </form>
</div>
@endsection
