@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">lista de usuarios</h1>
<a class="btn btn-primary" href="/students/create">crear nuevo estudiante</a>
<table class="table">
  <tr>
    <td>nombre</td>
    <td>apellidos</td>
    <td>fecha de nacimiento</td>
    <td>direccion</td>
    <td>email</td>
  </tr>

  @foreach($students as $student)
  <tr>
    <td>{{$student->name}}</td>
    <td>{{$student->apellidos}}</td>
    <td>{{$student->fecha_nacimiento}}</td>
    <td>{{$student->direccion}}</td>
    <td>{{$student->email}}</td>
    <td> <a class="glyphicon glyphicon-eye-open" href="/students/{{ $student->id }}"></a></td>

    @endforeach
  </tr>
</table>

<div>{{ $students->render() }} </div>
@endsection
