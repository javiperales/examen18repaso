@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">creacion de estudiantes</h1>
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
<div class="container">
    <form class="form" method="post" action="/students">
        {{csrf_field()}}
        <div class="form-group">
            <label>nombre</label>
            <input class="form-control" type="text" name="name" value="{{old('name')}}">
            @if( $errors->first('name'))
            <script>
                alert('el campo nonbre es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('name')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>apellidos</label>
            <input class="form-control" type="text" name="apellidos" value="{{old('apellidos')}}">
            @if( $errors->first('apellidos'))
            <script>
                alert('el campo apellidos es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('apellidos')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>fecha_nacimiento</label>
            <input class="form-control" type="date" name="fecha_nacimiento" value="{{old('fecha_nacimiento')}}">
            @if( $errors->first('fecha_nacimiento'))
            <script>
                alert('el campo fecha de nacimiento es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('fecha_nacimiento')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>direccion</label>
            <input class="form-control" type="text" name="direccion" value="{{old('direccion')}}">
            @if( $errors->first('direccion'))
            <script>
                alert('el campo direccion es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('direccion')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>email</label>
            <input class="form-control" type="text" name="email" value="{{old('email')}}">
            @if( $errors->first('email'))
            <script>
                alert('el campo email es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('email')}}
            </div>
            @endif
        </div>
        <input class="btn btn-primary" type="submit" name="nuevo" value="nuevo">
    </form>
</div>
@endsection
