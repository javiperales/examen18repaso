@extends('layouts.app')

@section('content')
<h1 class="alert alert-info" >detalle del estudiante {{$students->id}}</h1>

<ul class="list-group">
    <li class="list-group-item">nombre: {{$students->name}}</li>
    <li class="list-group-item">apellidos: {{$students->apellidos}}</li>
    <li class="list-group-item">fecha nacimiento: {{$students->fecha_nacimiento}}</li>
    <li class="list-group-item">direccion : {{$students->direccion}}</li>
    <li class="list-group-item">email: {{$students->email}}</li>

</ul>

@endsection
