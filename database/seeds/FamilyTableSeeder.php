<?php

use Illuminate\Database\Seeder;

class FamilyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('families')->insert([
            'code' => 'IFC',
            'name' => 'informatica',
        ]);
        DB::table('families')->insert([
            'code' => 'IMP',
            'name' => 'imagen personal',

        ]);
        DB::table('families')->insert([
            'code' => 'ADG',
            'name' => ' Administracion y Gestion',
        ]);



    }
}
