<?php

use Faker\Generator as Faker;

$factory->define(App\Question::class, function (Faker $faker) {
    $items = ['a', 'b', 'c', 'd'];
    return [
        'text' => $faker->sentence,
        'a' => $faker->sentence,
        'b' => $faker->sentence,
        'c' => $faker->sentence,
        'd' => $faker->sentence,
        'answer' => $items[rand(0, count($items) - 1)],
        'module_id' => rand(1, 4),
    ];
});
